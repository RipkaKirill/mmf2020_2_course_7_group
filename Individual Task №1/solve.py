import math

def check_perfect(Number):
    total = 1

    for i in range(2, int(math.sqrt(Number)) + 1):
        if (Number % i == 0) :
            total += i + Number // i

    if (Number == math.sqrt(Number) ** 2):
        total -= math.sqrt(Number)
        
    if (total == Number):
        return [Number, "True"]
    else: 
        return [Number, "False"]
