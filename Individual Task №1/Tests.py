import Test
from solve import check_perfect

Test.assert_equals(check_perfect(6), True)
Test.assert_equals(check_perfect(28), True)
Test.assert_equals(check_perfect(496), True)
Test.assert_equals(check_perfect(8128), True)
Test.assert_equals(check_perfect(33550336), True)
Test.assert_equals(check_perfect(12), False)
Test.assert_equals(check_perfect(97), False)
Test.assert_equals(check_perfect(481), False)
Test.assert_equals(check_perfect(1001), False)
Test.assert_equals(check_perfect(55555), False)