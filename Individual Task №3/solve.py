def binMechanism(arr):
    ans = []

    for i in range(0, len(arr)):
        str = arr[i]
        if (str.count("0") + str.count("1") == 8 and len(str) == 8):
            sizeOne = str.count("1")

            if (sizeOne % 2 == 0):
                ans.append(0)
            else:
                ans.append(1)
        else:
            pass

    return ans

    