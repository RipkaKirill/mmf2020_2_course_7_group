import test
from solve import binMechanism

test.assert_equals(binMechanism(["11001010", "11100000", ""]),  [0, 1])
test.assert_equals(binMechanism(["11001010", "11100010", ""]),  [0, 0])

