# Вычислительная практика

# Рипка Кирилл, 2 курс, 7 группа

# Индивидуальные задания:
* ### Инд. зад. №1 [вариант 6](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Individual%20Task%20%E2%84%961/)
* ### Инд. зад. №2 [вариант 5](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Individual%20Task%20%E2%84%962/)
* ### Инд. зад. №3 [вариант 3](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Individual%20Task%20%E2%84%963/)
# Групповое задание:
* ### Неоптимизированный бэкэнд под сайт с отелями + тест [Unoptimized](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Unoptimized/)
* ### Оптимизированный бэкэнд под сайт с отелями [Optimized](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Optimized/)
* ### Результаты скорости выполнения оптимизации [Result](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Result.xlsx)
* ### Сайт записей на отель [WebPage with Flask](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/WEB/)
* ### Визуализация результатов [Visualization](https://bitbucket.org/RipkaKirill/mmf2020_2_course_7_group/src/master/Visualization/)
* ### [Перейти на сайт](https://bookingbyvaleria.herokuapp.com)





