from datetime import datetime
import timeit
import time 

def main():
    file = "out1000.csv"
    dataFormat = '%Y-%m-%d %H:%M:%S'

    ResourseID = 689
    StartDateTime = "2021-06-01T21:00:00.000Z"
    StartDateTime = StartDateTime.replace('T', ' ')[:StartDateTime.rfind('.')]
    StartDateTime = datetime.strptime(StartDateTime, dataFormat)
    StartDateTime = int(time.mktime(StartDateTime.timetuple())) // 100

    EndDateTime = "2023-09-07T21:00:00.000Z"
    EndDateTime = EndDateTime.replace('T', ' ')[:EndDateTime.rfind('.')]
    EndDateTime = datetime.strptime(EndDateTime, dataFormat)
    EndDateTime = int(time.mktime(EndDateTime.timetuple())) // 100

    data = parseInput(read(file), dataFormat)
    return [data, ResourseID, StartDateTime, EndDateTime]



def read(file):
    f = open(file)
    
    data = f.read()

    f.close()

    return data

def parseString(str):
    return str.split(",")

def printArray(arr):
    for i in arr:
        print(i)

def parseInput(data, dataFormat):

    dict = {}

    # newData = []

    data = data.split('\n')
        
    for i in range(1, len(data) - 1):

        data[i] = data[i].split(',')
        data[i][1] = data[i][1].replace('T', ' ')[:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2].replace('T', ' ')[:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)

        #делим на 100 --> меленькие числа быстрее сравниваются
        unix1 = int(time.mktime(data[i][1].timetuple())) // 100
        unix2 = int(time.mktime(data[i][2].timetuple())) // 100

        # newData.append([int(data[i][0]), unix1, unix2])

        st = str(unix1) + "," + str(unix2)

        if data[i][0] in dict:
            dict.update({data[i][0]:dict[data[i][0]] + "," + st})
        else:
            dict.update({data[i][0]:st})
       
        # возможно будем делать все через  list
    return dict

def searchId(dict, StartDateTime, EndDateTime):
    ids = set()
    closed = set()

    for i in dict:
        arr = dict[str(i)].split(",")
        for val in range(0, len(arr), 2):
            # print(i)
            if (i in closed) or check([int(arr[val]), int(arr[val + 1])], StartDateTime, EndDateTime):
                closed.add(i)
                if val in ids:
                    ids.remove(i)
            else:
                ids.add(i)
    
    return ids

def findById(dict, ResourseID, StartDateTime, EndDateTime):
    arr = dict[str(ResourseID)].split(",")

    for i in range(0, len(arr), 2):
        if (check([int(arr[i]), int(arr[i + 1])], StartDateTime, EndDateTime)):
            return False

    return True   
    # print (dict[ResourseID])



def check(hotel, dateStart, dateEnd):
  return (
    (hotel[0] <= dateStart < hotel[1]) 
    or  
    (hotel[0] < dateEnd <= hotel[1]) 
    or
    (dateStart <= hotel[0] < dateEnd) 
    or
    (dateStart < hotel[1] <= dateEnd)
  )


# dict = parseInput(read())

# print(findById(dict, ResourseID, StartDateTime, EndDateTime))
# printArray(searchId(dict, StartDateTime, EndDateTime))

if __name__ == "__main__":
    val = main()
    findById(val[0], val[1], val[2], val[3])
    print(timeit.timeit("findById(val[0], val[1],val[2], val[3])", setup="from __main__ import findById  , val", number=1))

