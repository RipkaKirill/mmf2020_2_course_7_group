def assert_equals(ansFunct, ans):
    temp = []
    flag = True

    if (len(ansFunct) != len(ans)):
        print(False)
        return

    for i in range(0, len(ans)):
        temp.append(0)

    for i in range(0, len(ans)):
        for j in range(0, len(ans)):
            if (temp[j] == 0 and ans[j][0] == ansFunct[i][0] and ans[j][1] == ansFunct[i][1] and ans[j][2] == ansFunct[i][2] and ans[j][3] == ansFunct[i][3]):
                temp[j] = 1

    for i in range(0, len(temp)):
        if (temp[i]):
            pass
        else:
            flag = False

    print(flag)