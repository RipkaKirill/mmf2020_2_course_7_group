def quadruplets(t, value):
    t.sort()

    ans = []

    for i in range(0, len(t) - 3):
        for j in range(i + 1, len(t) - 2):
            for k in range(j + 1, len(t) - 1):
                for q in range(k + 1, len(t)):
                    # print(1)
                    if (t[i] + t[j] + t[k] + t[q] == value):
                        if (len(ans) == 0):
                            ans.append([t[i], t[j], t[k], t[q]])
                        else:
                            flag = True

                            for z in range(0, len(ans)):
                                if (ans[z][0] == t[i] and ans[z][1] == t[j] and ans[z][2] == t[k] and ans[z][3] == t[q]):
                                    flag = False
                                    break

                            if (flag):
                                ans.append([t[i], t[j], t[k], t[q]])

    return ans
